Add HxUI theme for [ng-select](https://ng-select.github.io/ng-select#/data-sources) to your project.

## Getting started
### Step 1. Install package:

```shell
npm install @hxui/ng-select --save
```

### Step 2. Import theme to project styles file `styles.scss`:
```scss
@import "@hxui/ng-select/themes/hxui.theme.scss";
```

## Demo

See [examples](https://angular.hxui.io/#/ng-select)

## Changelog

See [changelog](https://bitbucket.org/md-design/angular-hxui/src/master/projects/ng-select/CHANGELOG.md)