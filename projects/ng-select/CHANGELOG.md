## [1.1.0](https://www.npmjs.com/package/@hxui/ng-select/v/1.1.0) (2022-03-28)

### Enhancement

- Added support for `is-outlined`, `is-badge` and `is-rounded` multiselect classes. Note that `is-badge` aligns with `hx-badge`.
