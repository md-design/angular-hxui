export enum DatepickerViewModeEnum {
  Days,
  Months,
  Years
}
