export interface ISelectizeItem {
  label: string;
  value: string;
  disabled?: boolean;
}
