export enum FilterType {
  SingleSelect,
  Search,
  MultiSelect
}
